<?php

require_once('lib/nusoap.php'); 

$server = new nusoap_server;

$server->configureWSDL('server', 'urn:server');

$server->wsdl->schemaTargetNamespace = 'urn:server';

//SOAP complex type return type (an array/struct)

$server->wsdl->addComplexType(
    'Person',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'id_user' => array('name' => 'id_user', 'type' => 'xsd:int'),
        'fullname' => array('name' => 'fullname', 'type' => 'xsd:string'),
        'email' => array('name' => 'email', 'type' => 'xsd:string'),
        'level' => array('name' => 'level', 'type' => 'xsd:int')
    )
);

$server->wsdl->addComplexType('Contact',
    'complexType',
    'struct',
    'all',
    '',
    array(
            'id' => array('name' => 'id', 'type' => 'xsd:int'),
            'first_name' => array('name' => 'first_name', 'type' => 'xsd:string'),
            'last_name' => array('name' => 'last_name', 'type' => 'xsd:string'),
            'email' => array('name' => 'email', 'type' => 'xsd:string'),
            'phone_number' => array('name' => 'phone_number', 'type' => 'xsd:string')
    )
);
$server->wsdl->addComplexType('AddressBook',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
    array(),
    array(
            array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Contact[]')
    ),
    'tns:Contact'
);

//first simple function
$server->register('consulta',
			array('rfc' => 'xsd:string','nombre' => 'xsd:string'),  //parameter
			array('return' => 'xsd:Array'),  //output
			'urn:server',   //namespace
			'urn:server#consultaServer',  //soapaction
			'rpc', // style
			'encoded', // use
			'Just say hello');  //description

//this is the second webservice entry point/function 
$server->register('login',
			array('username' => 'xsd:string', 'password'=>'xsd:string'),  //parameters
			array('return' => 'tns:AddressBook'),  //output
			'urn:server',   //namespace
			'urn:server#loginServer',  //soapaction
			'rpc', // style
			'encoded', // use
			'Check user login');  //description

//first function implementation
function consulta($username) {
	$items = array();
	$items[]="ola1";
	$items[]="la2";
	return $items;
        //return 'Howdy, '.$username.'!';
}

//second function implementation 
function login($username, $password) {
        //should do some database query here
        // .... ..... ..... .....
        //just some dummy result
		$items=array();
        $items[] = array(
		'id_user'=>1,
		'fullname'=>'John Reese',
		'email'=>'john@reese.com',
		'level'=>99	);
        $items[] = array(
		'id_user'=>1,
		'fullname'=>'John Reese',
		'email'=>'john@reese.com',
		'level'=>99	);
		return $items;
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';

$server->service($HTTP_RAW_POST_DATA);

?>